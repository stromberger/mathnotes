# Mikroökonomie Formelsammlung

## Nutzenmaximierung

### KKT (inneres Optimum)
```math
\frac{\frac{\partial u(x^*)}{\partial x_1}}{\frac{\partial u(x^*)}{\partial x_2}}=\frac{p_1}{p_2}
```

## Produktion und Kostenminimierung

### Produktionsfunktion
$`f(z)`$ mit $`z=(z_1, \dots, z_m)`$ gibt höchste Menge an, die ein Unternehmen mit jeder gegebenen Kombination von Inputs bei gegebener Produktionstechnologie produzieren kann.

Es gilt für den Output $`q \leq f(z)`$.

### Durchschnittsproduktivität der i-ten Faktors
```math
AP_i(z) = \frac{f(z)}{z_i}
```

### Elastizität in Bezug auf den i-ten Input
```math
\mu_i(z) = \frac{\partial f(z)}{\partial z_i} \frac{z_i}{f(z)} = \frac{\partial ln(f(z))}{\partial ln(z_i)}
```

### Grenzproduktivität der i-ten Faktor
```math
MP_i(z) = \frac{\partial f(z)}{\partial z_i}
```

### Langfristiges Kostenminimierungsproblem (LKMP)
```math
\min_{z \geq 0}(w \cdot z) \quad \text{s.t.} \quad f(z) \geq q
```

### bedingte Faktornachfragekorrespondenz
$`z(w,q)`$ ordnet jeder Kombination $`(w,q)`$ mit $`w \gg 0 \land q \geq 0`$ die entsprechende Menge der optimalen Faktorkombinationen des LKMP zu.

Die **bedingte Faktornachfragefunktion** besteht aus nur einem Element.

### langfristige Kostenfunktion
$`c(w,q)`$ ist die Wertfunktion des LKMP:
```math
c(w,q) = \min_{z \geq 0}(w \cdot z) \quad \text{s.t.} \quad f(z) \geq q
```

### langfristige Grenzkosten
```math
MC(w,q) = \frac{\partial c(w,q)}{\partial q}
```

### kurzfristige Grenzkosten
```math
MC^s(w,q) = \frac{dc^s(q)}{dq}
```

### langfristige Durchschnittskosten
```math
AC(w,q) = \frac{c(w,q)}{q}
```
